import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun test0 (){
        assertEquals(0.0,calculIncrement(0.0,0.0))
    }
    @Test
    fun testRealistic(){
        assertEquals(27.9, calculIncrement(25.7,2.2))
    }
    @Test
    fun testNotRealistic (){
        assertEquals(2.222222211E9,calculIncrement(1234567890.0,987654321.0))
    }
}