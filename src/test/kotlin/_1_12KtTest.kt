import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class _1_12KtTest{
    @Test
    fun test0(){
        assertEquals(32.0,farenheit(0.0))
    }
    @Test
    fun test1(){
        assertEquals(33.8,farenheit(1.0))
    }
    @Test
    fun test2(){
        assertEquals(35.6,farenheit(2.0))
    }
}