import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{
    @Test
    fun test0(){
        assertEquals(0, dobleNumeros(0))
    }
    @Test
    fun test1(){
        assertEquals(2, dobleNumeros(1))
    }
    @Test
    fun testNegativeNumber(){
        assertEquals(-4, dobleNumeros(-2))
    }
}