import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class _1_9KtTest{
    @Test
    fun testPreuIgualPreuAmbDescompte (){
        assertEquals(0.0, calculPercentatge(5.0,5.0))
    }
    @Test
    fun test80I100 (){
        assertEquals(20.0, calculPercentatge(100.0,80.0))
    }
    @Test
    fun test40I60 (){
        assertEquals(33.33333333333334, calculPercentatge(60.0,40.0))
    }
}