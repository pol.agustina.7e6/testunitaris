import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{
    @Test
    fun test0(){
        assertEquals(0, sumaDosNumerosEnters(0,0))
    }
    @Test
    fun testPositiveAndNegaiveNumber(){
        assertEquals(0, sumaDosNumerosEnters(1,-1))
    }
    @Test
    fun testBigNumbers(){
        assertEquals(1000000000, sumaDosNumerosEnters(300000000,700000000))
    }
}