import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun test0(){
        assertEquals(1,add1(0))
    }
    @Test
    fun test1(){
        assertEquals(2,add1(1))
    }
    @Test
    fun test2(){
        assertEquals(3,add1(2))
    }
}