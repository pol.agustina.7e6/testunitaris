import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class _1_8KtTest{
    @Test
    fun test0(){
        assertEquals(0.0, multiplicaPerDos(0.0))
    }
    @Test
    fun test1andAQuarter(){
        assertEquals(2.5, multiplicaPerDos(1.25))
    }
    @Test
    fun test1andAHalf(){
        assertEquals(3.0, multiplicaPerDos(1.5))
    }
}