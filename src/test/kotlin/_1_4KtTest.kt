import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun test0(){
        assertEquals(0,areaRectangle(0,0))
    }
    @Test
    fun test0AndInt(){
        assertEquals(0,areaRectangle(0,5))
    }
    @Test
    fun testNegativeAndPositiveNumber(){
        assertEquals(25,areaRectangle(-5,5))
    }
}