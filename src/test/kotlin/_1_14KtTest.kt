import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{
    @Test
    fun test0(){
        assertEquals(Double.NaN, calculaPreu(0,0.0))
    }
    @Test
    fun testRealistic(){
        assertEquals(16.666666666666668, calculaPreu(12,200.0))
    }
    @Test
    fun testNotRealistic(){
        assertEquals(1.0000000071E8, calculaPreu(123456789, 1.234567898765432E16))
    }
}