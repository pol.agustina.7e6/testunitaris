import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun test0(){
        assertEquals(0, pupitres(0,0,0))
    }
    @Test
    fun testRealista(){
        assertEquals(47, pupitres(31,30,31))
    }
    @Test
    fun testNoRealista(){
        assertEquals(1569751344, pupitres(651456836,1346531637,1141514214))
    }
}