import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class _1_16KtTest{
    @Test
    fun test0(){
        assertEquals(0.0, double(0))
    }
    @Test
    fun test1(){
        assertEquals(1.0, double(1))
    }
    @Test
    fun test2(){
        assertEquals(2.0, double(2))
    }
}