import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

internal class _1_11KtTest{
    @Test
    fun test0(){
        assertEquals(0.0, calculVolum(0.0,0.0,0.0))
    }
    @Test
    fun test1(){
        assertEquals(1.0, calculVolum(1.0,1.0,1.0))
    }
    @Test
    fun test2(){
        assertEquals(8.0, calculVolum(2.0,2.0,2.0))
    }
}