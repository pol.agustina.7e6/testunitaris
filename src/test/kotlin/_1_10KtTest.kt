import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun test0 (){
        assertEquals(0.0, midaPizza(0.0))
    }
    @Test
    fun testRealistic (){
        assertEquals(463.76976150455926,midaPizza(24.3))
    }
    @Test
    fun testNotRealistic (){
        assertEquals(1.197070795767721E16,midaPizza(123456789.0))
    }
    @Test
    fun test2 (){
        assertEquals(Math.PI,midaPizza(2.0))
    }
}