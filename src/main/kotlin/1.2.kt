/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Dobla l'enter
*/
import java.util.*

fun main()
{
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val numero = scanner.nextInt()
    println("Aquest és el seu doble: ")
    println(dobleNumeros(numero))
}

fun dobleNumeros(num: Int): Int{
    return num*2
}