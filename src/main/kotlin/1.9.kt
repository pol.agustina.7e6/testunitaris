/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Calcula el descompte
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el preu d'un producte abans i despres d'aplicar un descompte:")
    val preu = scanner.nextDouble()
    val preuAmbDescompte = scanner.nextDouble()
    println("Aquest és el percentatge del descompte: ")
    println(calculPercentatge(preu,preuAmbDescompte))
}

fun calculPercentatge(preu:Double,preuAmbDescompte:Double):Double{
    return (100 - 100*(preuAmbDescompte/preu))
}