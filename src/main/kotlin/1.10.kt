/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Quina és la mida de la meva pizza?
*/
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el diametre de la pizza:")
    val diametre = scanner.nextDouble()
    print("La seva superficie es ${midaPizza(diametre)}")
}

fun midaPizza (diametre:Double):Double{
    val radi = diametre/2.0
    return (radi*radi)*Math.PI
}