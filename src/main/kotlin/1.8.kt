/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Dobla el decimal
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val numero = scanner.nextDouble()
    println("Aquest és el seu doble: ")
    println(multiplicaPerDos(numero))
}

fun multiplicaPerDos(num:Double):Double{
    return num*2.0
}