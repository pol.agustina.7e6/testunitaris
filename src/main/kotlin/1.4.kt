import java.util.*
import kotlin.math.abs

/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Calcula l’àrea
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix les mides:")
    val llargada = scanner.nextInt()
    val amplada = scanner.nextInt()
    println("Aquesta és la seva àrea:")
    println(areaRectangle(llargada,amplada))
}
fun areaRectangle(B: Int, A: Int): Int{
    //Una area no pot ser negativa
    return abs(B*A)
}