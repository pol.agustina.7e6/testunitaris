import java.util.*

/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Operació boja
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix quatre valors:")
    val a = scanner.nextInt()
    val b = scanner.nextInt()
    val c = scanner.nextInt()
    val d = scanner.nextInt()
    println("Aquesta és el resultat de l'operació boja:")
    println(operacioBoja(a,b,c,d))
}

fun operacioBoja(a:Int,b:Int,c:Int,d:Int):Int{
    return (a + b) * (c % d)
}