/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Afegeix un segon
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número de segons menor que 60:")
    val segons = scanner.nextInt()
    println("Afegim un segon: ${addSecond(segons)}")
}

fun addSecond (segons:Int):Int = (segons+1)%60