/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Transforma l’enter
*/
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un enter:")
    val enter = scanner.nextInt()
    println(double(enter))
}

fun double (int: Int):Double{
    return int.toDouble()
}