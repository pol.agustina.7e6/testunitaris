/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Divisor de compte
*/
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els començals i el preu a pagar:")
    val comencals = scanner.nextInt()
    val preu = scanner.nextDouble()
    print(calculaPreu(comencals,preu))
    print("€")
}

fun calculaPreu(comencals:Int,preu:Double):Double = preu/comencals