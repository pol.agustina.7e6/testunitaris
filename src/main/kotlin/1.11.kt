/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Calculadora de volum d’aire
*/
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix les mides de l'habitació:")
    val amplada = scanner.nextDouble()
    val llargada = scanner.nextDouble()
    val alcada = scanner.nextDouble()
    println("Aquest és el seu volum d'aire: ")
    println(calculVolum(amplada,llargada,alcada))
}

fun calculVolum(x:Double,y:Double,z:Double):Double{
    return x*y*z
}