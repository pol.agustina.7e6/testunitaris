/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Quina temperatura fa?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una temperatura i el seu increment:")
    val temperatura = scanner.nextDouble()
    val increment = scanner.nextDouble()
    print("La temperatura actual és ")
    print(calculIncrement(temperatura,increment))
}

fun calculIncrement (temp: Double, tempInc:Double):Double = temp+tempInc