/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Número següent
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val numero = scanner.nextInt()
    print("Després ve el ")
    println(add1(numero))
}

fun add1 (num:Int):Int{
    return num+1
}