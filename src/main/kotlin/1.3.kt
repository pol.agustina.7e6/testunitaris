import java.util.*

/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Suma de dos nombres enters
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix dos números:")
    val numero1 = scanner.nextInt()
    val numero2 = scanner.nextInt()
    println("Aquesta és la seva suma:")
    println(sumaDosNumerosEnters(numero1,numero2))
}

fun sumaDosNumerosEnters(num1:Int,num2:Int):Int{
    return num1+num2
}