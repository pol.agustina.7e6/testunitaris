/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: De Celsius a Fahrenheit
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els graus celcius:")
    val celcius = scanner.nextDouble()
    println("Aquest es el seu valor en Farenheits:")
    println(farenheit(celcius))
}

fun farenheit(celcius:Double):Double{
    return ((celcius*9.0/5.0)+32.0)
}