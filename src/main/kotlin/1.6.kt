import java.util.*

/*
* AUTHOR: Pol Agustina Prats
* DATE: 19/09/2022
* TITLE: Pupitres
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la cantitat d'alumnes de cada classe:")
    val classe1 = scanner.nextInt()
    val classe2 = scanner.nextInt()
    val classe3 = scanner.nextInt()
    print("Es necessiten ")
    print(pupitres(classe1,classe2,classe3))
    print(" pupitres.")
}

fun pupitres(c1:Int,c2:Int,c3:Int):Int{
    return (c1/2 + c2/2 + c3/2 + c1%2 + c2%2 + c3%2)
}